class AddProgressPercentageToTask < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :progress_percentage, :integer
  end
end
