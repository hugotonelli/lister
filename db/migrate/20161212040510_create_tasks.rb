class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :description, limit: 255
      t.integer :status
      t.integer :priority
      t.string :type

      t.timestamps
    end
  end
end
