class TaskList < ApplicationRecord
    has_many :tasks
    
    validates :name, presence: true
    validates :slug, presence: true
end
