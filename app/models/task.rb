class Task < ApplicationRecord
    belongs_to :task_list
    
    enum priority: [:high, :medium, :low]
    enum status: [:pending, :done, :expired, :ongoing]
    
    validates :description, length: { maximum: 255 }, presence: true
    validates :status, presence: true
    validates :priority, presence: true
    validates :type, presence: true
    validates :task_list, presence: true
end
